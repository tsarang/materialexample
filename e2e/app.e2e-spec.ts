import { MaterialExamplePage } from './app.po';

describe('material-example App', () => {
  let page: MaterialExamplePage;

  beforeEach(() => {
    page = new MaterialExamplePage();
  });

  it('should display welcome message', done => {
    page.navigateTo();
    page.getParagraphText()
      .then(msg => expect(msg).toEqual('Welcome to app!!'))
      .then(done, done.fail);
  });
});
